#!/usr/bin/env python3
#
# Retrieve VISTA images in ESO Archive.
#
#  Sample code.
#
#  Since 2024/04/27 H. Akitaya (ARC/CIT)

# import VISTA class
from vista import VISTA

if __name__ == '__main__':

    # Crate instance of VISTA class.
    vista = VISTA(debug=False, cache=False)

    # debug: debug mode.
    # cache: ignore existence files in cache directory.

    # 1.(a) read (RA, dec) from the existent fits file.
    fn = 'uploaded_20240423_15501085.fits'
    vista.read_coord_from_fits(fn)

    # or
    # 1. (b) Set arbitrary (Ra, dec) with values.
    # vista.set_coord(ra=120., dec=-30.)

    # 2. Query archive information from ESO Archive.
    band = 'Ks'
    vista.query_archive(band=band)

    # 3. Obtain DP.IDs of the aimed field.
    list_dp_id = vista.get_first_field_id()

    # 4. Retrieve fits images.
    retrieved_fns = vista.retrieve_fits_images(list_dp_id)
