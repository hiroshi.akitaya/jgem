#!/usr/bin/env python3
#
# Access VISTA images in ESO Archive
#
#  Since 2024/04/27 H. Akitaya (ARC/CIT)

__author__ = 'Hiroshi Akitaya'
__date__ = '2024/04/27'
__version__ = '0.1.0'

__description__ = 'Retrieve Vista data'
__license__ = 'TBD'

import os
import sys
import io
import urllib

import requests

from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy.table import Table


class VISTA(object):
    def __init__(self, download_dir='./download/', cache=True, debug=False):
        self.coord = None
        self.query_table = None
        self.download_dir = download_dir
        self.cache = cache
        self.debug = debug  # Debug mode.

    def check_download_dir(self, create=False):
        if os.path.isdir(self.download_dir):
            return True
        else:
            if create:
                os.mkdir(self.download_dir)
                print(f'Created download directory: {self.download_dir}')
                return True
            return False

    def set_coord(self, ra, dec):
        self.coord = SkyCoord(ra, dec, unit='deg')

    def read_coord_from_fits(self, fn):
        with fits.open(fn) as hdulist:
            ra = hdulist[0].header['CRVAL1']
            dec = hdulist[0].header['CRVAL2']
            self.coord = SkyCoord(ra, dec, unit='deg')
            if self.debug:
                print(ra, dec)
        return self.coord

    def query_archive(self, band='Ks', dp_subtype='tile', sep=1.0):
        QUERY_FORMAT = 'csv'
        import urllib.parse
        URL_ESO_TAP_OBS = 'https://archive.eso.org/tap_obs/'
        ra = self.coord.ra.value
        dec = self.coord.dec.value
        # Create ADQL statement for query.
        # ADQL: https://archive.eso.org/tap_obs/examples
        adql_str = ("SELECT TOP 50 instrument_name, dp_id, s_ra, s_dec, em_min, em_max, filter, obs_collection,"
                    " dataproduct_type, dataproduct_subtype, t_exptime, s_resolution, "
                    f" distance(point('',s_ra,s_dec),point('',{ra},{dec})) as dist_from_cntr, abmaglim"
                    " FROM ivoa.Obscore WHERE dataproduct_type='image'"
                    " AND instrument_name='VIRCAM'"
                    f" AND INTERSECTS(CIRCLE('ICRS',{ra}, {dec},{sep}),s_region) = 1"
                    f" AND filter='{band}'"
                    f" AND dataproduct_subtype='{dp_subtype}'"
                    " ORDER BY dist_from_cntr ASC, t_exptime DESC"
                    )
        url_query = (URL_ESO_TAP_OBS +
                     f'sync?REQUEST=doQuery&LANG=ADQL&MAXREC=100&FORMAT={QUERY_FORMAT}&QUERY=' +
                     urllib.parse.quote(adql_str))
        if self.debug:
            print(url_query)

        # Query archive.
        r = requests.get(url_query)
        if self.debug:
            print(r.status_code, requests.codes.ok)
        #if r.status_code != requests.codes.ok:
        #    raise QueryFailedException(url_query, r.status_code)        print(r.content)

        # Read content as BytesIO.
        query_result_csv = io.BytesIO(r.content)
        query_result_csv.write(r.content)

        # Convert query result as astropy.table.Table
        self.query_table = Table.read(query_result_csv, format='csv')
        if self.debug:
            print(self.query_table)

    def get_first_field_id(self):
        if type(self.query_table) is not Table:
            return None
        return [self.query_table['dp_id'][0]]

    def retrieve_fits_images(self, list_dp_id):
        URL_ESO_DATAPORTAL = 'https://dataportal.eso.org/dataportal_new/file/'

        retrieved_fns = []

        # Check existence of download dir
        if not self.check_download_dir(create=True):
            raise IOError(f'download dir {self.download_dir} does not exist')

        # Retrieve fits files from ESO Archive
        for dp_id in list_dp_id:
            fn_fits = f'{self.download_dir}/{dp_id}.fits'
            # Check cache
            if self.cache:
                if os.path.isfile(fn_fits):
                    print(f'File {fn_fits} exists in cache. Skip.')
                    continue

            url_image_query = URL_ESO_DATAPORTAL + urllib.parse.quote(dp_id)
            print(f'Retrieving image {fn_fits} from {url_image_query}')
            r = requests.get(url_image_query)
            if r.status_code != requests.codes.ok:
                sys.stderr.write(f'{r.status_code}: Failed to retrieve image {dp_id}\n')
                continue

            with open(fn_fits, "wb") as f:
                f.write(r.content)
                print(f'File {fn_fits} written to disk.')
                retrieved_fns.append(fn_fits)

        return retrieved_fns

#class QueryFailedException(Exception):
#    sys.stderr.write('Query Failed\n')
#    sys.exit(1)



if __name__ == '__main__':
    band = 'Ks'
    fn = 'uploaded_20240423_15501085.fits'
    vista = VISTA(debug=True)
    vista.read_coord_from_fits(fn)
    #vista.set_coord(ra=120., dec=-30.)
    vista.query_archive(band=band)
    list_dp_id = vista.get_first_field_id()
    retrieved_fns = vista.retrieve_fits_images(list_dp_id)

    sys.exit(0)
