#!/usr/bin/env python
# #
# Retrieve DECaPS DR2 images
#
#   2024-04-24 H. Akitaya
#
#
#  Usage:
#       (1) get_decaps.py --jgem uploaded_20240423_15501085.fits
#       (2) get_decaps.py --ra-dec 120.0 -25.0 --band zY --fn-out output.fits
#


__author__ = "Hiroshi Akitaya"
__email__ = "akitaya@perc.it-chiba.ac.jp"

__version__ = "0.1.1"

# import io
import sys

import requests
import argparse
from astropy.io import fits
# from astropy.utils.data import download_file


def get_image(ra=0.0, dec=0.0, pixel_scale=0.262, bands="Y", fn_out="tmp.fits"):
    """Get DECaPS cutout image at RA and Dec."""
    # Access to DECaPS data; https://datalab.noirlab.edu/decaps/access.php
    url = (f"http://legacysurvey.org/viewer/fits-cutout/" +
           f"?layer=decaps2&ra={ra}&dec={dec}&pixscale={pixel_scale}&bands={bands}")
    r = requests.get(url)
    # print(r.headers['content-type'])
    print(f"Status: {r.status_code}")
    if r.status_code != requests.codes.ok:
        sys.stderr.write(f"{url} download error.\n")
        sys.exit(1)
    img = r.content
    with open(fn_out, "wb") as f:
        f.write(img)


def resolve_jgem_fits(fn):
    """Get RA, Dec, and object name from J-GEM image."""
    with fits.open(fn) as hdul:
        ra = hdul[0].header['CRVAL1']
        dec = hdul[0].header['CRVAL2']
        objname = hdul[0].header['OBJECT']
    return ra, dec, objname


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-j", "--jgem", default=None, help="JGEM image server fits file")
    arg_parser.add_argument("-b", "--bands", default="z", help="BANDS (e.g. z, Yz, Yzr)")
    arg_parser.add_argument("-p", "--pixel-scale", default=0.262, help="Pixel scale")
    arg_parser.add_argument("-f", "--fn-out", default=None, help="Output file name")
    arg_parser.add_argument("--ra-dec", default=(None, None), nargs=2, help="RA[deg] Dec[deg]")
    args = arg_parser.parse_args()

    bands = args.bands
    pixel_scale = args.pixel_scale
    fn_out = args.fn_out

    if args.jgem is not None:
        print("Reading JGEM image...")
        ra, dec, objname = resolve_jgem_fits(args.jgem)
        if fn_out is None:
            fn_out = f"{objname}_{bands}_DECAPS.fits"
    else:
        ra, dec = args.ra_dec

    print(f"Getting DECaPS cutout image centered at ({ra}, {dec}) as {fn_out} ...")
    get_image(ra=ra, dec=dec, pixel_scale=pixel_scale, bands=bands, fn_out=fn_out)
    print("Done.")
